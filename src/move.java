
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;





import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;


public class move extends JPanel implements ActionListener {

	public Timer t;
	public ArrayList<Rectangle> recs ;
	public int numOfSensors;
	public int radiusOfSensors;
	public int simulateTracker;
	public int currentSensorMoving;
	public sensors s;
	public int fieldSize;
	public int numberRandomX;
	public int extraSensors;
	public String algorithm;
	public int iterations;
	
	public move(int radius, int extra, String algo){
		iterations=1;
		algorithm = algo;
		extraSensors=extra;
		 numOfSensors=0;
	        radiusOfSensors=radius;
	        simulateTracker=0;
	        currentSensorMoving=0;
	     
	        fieldSize=500;
	        calculateSensorParameters();
	        System.out.println("radius of sensors:"+ radiusOfSensors + " number of sensors: "+ numOfSensors +" extra sensors: " + extraSensors );
	        System.out.println(" name of algorithm running:"+ algorithm);
	        System.out.println("Domain size: "+ fieldSize);
	        recs = new ArrayList<Rectangle>();
	        s= new sensors(fieldSize, numOfSensors+extraSensors, radiusOfSensors);
	        s.initiate();
	        
	        t= new Timer(500, this);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2= (Graphics2D) g;
		
		 Rectangle r = null;
		if(simulateTracker<1){
			 
			Random rn = new Random();
	        int x =0;
	       
	       
	        
	        int maximumXAllowed=500-(radiusOfSensors*2); 
	        //////////////////////////////////////////////calculation stuff end
	        
	        //makes rectangles within the interval, based on their calculated radius
	        for(int i=0; i<(numOfSensors+extraSensors);i++){
	        	x= rn.nextInt(maximumXAllowed - 0 + 1) + 0;
	        	//System.out.println(x);
	        	r = new Rectangle(x,100,(radiusOfSensors*2),100);
	        	recs.add(r);
	        }
	      
	       
	       sortRectangles();
	        
	      simulateTracker=1;
		
		}
	        
	        if(simulateTracker>0){
	        
	        	for(int i=0;i<(numOfSensors+extraSensors);i++){
	        		g2.draw(recs.get(i));
	        		 
		        	 
		        	 
		        }
	        	
	        }
	        
	        System.out.println("TOTAL MOVED:"+ s.getMovement());
	        
	       
	     
	        if(s.checkTracker()==false && iterations!=0){
		        t.start();
		        }
      
	      
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		 
		 
		 if(algorithm.equals("rigid")){
			 System.out.println(" Running rigid");
			 BasicRigid();
		 }
		 if(algorithm.equals("simple")){
			 System.out.println(" Running simple");
			 simpleCoverage();
		 }
		 
		 if((s.checkTracker()==true && iterations!=0 )|| ((s.allSensorsMoved()==true))){
		        s.updateIterations();
	 			iterations=iterations-1;
	 			System.out.println("new positions of sensors:");
	 			 for(int i=0;i<recs.size();i++){
	 	        	System.out.println(recs.get(i).getX());
	 	        }
	 			resetForIterations();
				 
		        }
		
		if(iterations!=0){
			repaint();
		}
	    
		if( iterations ==0){
		  for(int i=0;i<s.getIterations().size();i++){
      		//System.out.println(" Iteration " + i + " "+ s.getIterations().get(i));
      	}
		  s.averageIterationData();		 	 
		  s.outputData();
		 
		}
		
		 if(iterations==0){
			  
	        	t.stop();
	        }
		
	}
	//////////////////////////////////////////////////////////
	public void resetForIterations(){
		recs.clear();
		simulateTracker=0;
		currentSensorMoving=0;
		s.resetForNewIteration();
		System.out.println(" resetForIterations");
	}
	//////////////////////////////////////////////////////////////
	
	 public void BasicRigid(){
		 
		 int currentUncoveredPosition=s.nextUncoveredXposition();
		 int totalMoved=0;
	    	System.out.println("Current Uncovered position:" +currentUncoveredPosition );
	    	
	    
	   
	    				totalMoved=(int) (Math.abs(currentUncoveredPosition-recs.get(currentSensorMoving).getX()));
	    				s.updateMovement(totalMoved);
	    				
			     		recs.get(currentSensorMoving).setLocation(currentUncoveredPosition, (int) recs.get(currentSensorMoving).getY()); 
			      		currentSensorMoving++;
			     		s.updateTrackerAtPosition(radiusOfSensors*2);
			    		 
			
	    		
	    		
	    		
	    	
		 
	 }
	 ///////////////////////////////////////////////////////////
	
	 /////////////////////////////////////////////////////////////
	 public void simpleCoverage(){
		     
			 s.resetTracker();
			 
			 for(int i=0;i<recs.size();i++){
				 s.updateTrackerPositions(radiusOfSensors*2, (int) recs.get(i).getX()); 
			 }
			 
			 if (s.allSensorsMoved()==true){
				 t.stop();
				 return;
			 }
			
			
			 int currentUncoveredPosition=s.nextUncoveredXposition();
			 int totalMoved=0;
		    System.out.println("Current Uncovered position:" +currentUncoveredPosition );
		     int nextSensorDistance=0;
		     int closestSensor=closestSensor();
		     int nextClosestSensorID= (nextClosestSensor(currentUncoveredPosition));
		     
		    		if(currentUncoveredPosition<0){
		    			t.stop();
		    			
		    			return;
		    		}
		    	
		    		// if the uncovered area is to the left of the sensor
		    		if(currentUncoveredPosition<=recs.get(closestSensor).getX()){
		    			totalMoved=(int) (Math.abs(currentUncoveredPosition-recs.get(closestSensor).getX()));
		    			recs.get(closestSensor).setLocation(currentUncoveredPosition, (int) recs.get(closestSensor).getY());
		    			
		    		}
		    		
		    		if(currentUncoveredPosition>recs.get(closestSensor).getX()){
		    			System.out.println("closest sensor X:" + recs.get(closestSensor).getX());
		    			System.out.println("Current Uncovered position:" +currentUncoveredPosition );
		    			
		    			if(notLargestPosition((int) recs.get(closestSensor).getX()) == true){//if the sensor is not at largest x position
		    				//if the distance between uncovered position and next sensor is smaller than diameter of sensors
		    				if((Math.abs(currentUncoveredPosition-nextClosestSensorID)< (radiusOfSensors*2))){
		    					System.out.println("Next closest sensor to Uncovered position X:" +nextClosestSensorID );
			    				nextSensorDistance = (int)(Math.abs(currentUncoveredPosition-nextClosestSensorID));
			    				System.out.println("distance between next closest sensor and uncovered position:" +nextSensorDistance );
			    				nextSensorDistance=(int)(Math.abs(nextSensorDistance-(radiusOfSensors*2)));
			    				nextSensorDistance=(int)(Math.abs(nextSensorDistance-currentUncoveredPosition));
			    				totalMoved=(int) (Math.abs(nextSensorDistance-recs.get(closestSensor).getX()));
			    				recs.get(closestSensor).setLocation(nextSensorDistance, (int) recs.get(closestSensor).getY());
			    				System.out.println("new X for closest sensor:" +nextSensorDistance );
		    				}
		    				if((Math.abs(currentUncoveredPosition-nextClosestSensorID)>= (radiusOfSensors*2))){//if the sensor beyond uncovered larger than diameter
		    					totalMoved=(int) (Math.abs(currentUncoveredPosition-recs.get(closestSensor).getX()));
			    				recs.get(closestSensor).setLocation(currentUncoveredPosition, (int) recs.get(closestSensor).getY());
		    				}
		    			}
		    			
		    			if(notLargestPosition((int) recs.get(closestSensor).getX()) == false){// if the sensor is at largest position
		    				//totalMoved=(int) (Math.abs(currentUncoveredPosition-recs.get(closestSensor).getX()));
		    				nextSensorDistance = (int)(Math.abs(currentUncoveredPosition-fieldSize));
		    				nextSensorDistance=(int)(Math.abs(nextSensorDistance-(radiusOfSensors*2)));
		    				nextSensorDistance=(int)(Math.abs(nextSensorDistance-currentUncoveredPosition));
		    				totalMoved=(int) (Math.abs(nextSensorDistance-recs.get(closestSensor).getX()));
		    				recs.get(closestSensor).setLocation(nextSensorDistance, (int) recs.get(closestSensor).getY());
		    				System.out.println("new X for closest sensor near end of field:" +nextSensorDistance );
		    			}
		    			
		    		}
		    	
		    		
		    		System.out.println("currently moved:"+ totalMoved);
    				s.updateMovement(totalMoved);
	    		
		    				
				
		    		
		    		
		    	
		    	
	 }
	 //////////////////////////////////////////////////////////
	 
	
	 
	 /////////////////////////////////////////////////////////
	 public int closestSensor(){
		 int nextUncoveredPosition=s.nextUncoveredXposition();
		 int currentClosestSensor=0;
		 int distanceFromUncovered=0;
		 int previousDistanceFromUncovered=fieldSize;// set to fieldSize so that the first sensor checked is not ignored in the loop
		 
		 for(int i=0;i<recs.size();i++){
			 if(s.sensorMoved(i)==false){
			 distanceFromUncovered=(int)(Math.abs(nextUncoveredPosition-recs.get(i).getX()));
			 
			 if(distanceFromUncovered< previousDistanceFromUncovered){
				 currentClosestSensor=i;
			 }
			
			 previousDistanceFromUncovered = distanceFromUncovered;
			 }
		 }
		 s.updateSensors(currentClosestSensor);
		 System.out.println("currentClosestSensor"+currentClosestSensor);
		 return currentClosestSensor;
	 }
	 
	 ////////////////////////////////////////////////////////////
	 public boolean notLargestPosition(int position){
		 for(int i=0;i<recs.size();i++){
	    		if(recs.get(i).getX()>position){
	    			return true;
	    		}
	    	}
		 return false;
	 }
	/////////////////////////////////////////////////////////////
	 //finds the next closest sensor to current closest sensor that needs to cover an uncovered area. 
	 public int nextClosestSensor(int currentUncoveredPosition){
		 ArrayList<Rectangle> tempRecs = recs ;
		 int nextClosestSensorIDposition=0;
		 
		
		 
		 int currentPositionToChange=0;
	    	int currentSmallestIndex=0;
	    	
	    	for(int i=0;i<tempRecs.size();i++){
	    		currentPositionToChange=i;
	    		currentSmallestIndex=i;
	    		
	    			for(int j=i;j<tempRecs.size();j++){
	    				//currentSmallestIndex=recs.get(j).getX();
	    				if(tempRecs.get(j).getX()< tempRecs.get(currentSmallestIndex).getX()){
	    				currentSmallestIndex=j;
	    				}
	    			}
	    			Rectangle temp = tempRecs.get(i);//saving the rectangle at current position
	    			//replacing the rectangle at current position with smaller x position rectangle
	    			tempRecs.set(i, tempRecs.get(currentSmallestIndex));
	    			tempRecs.set(currentSmallestIndex, temp);
	    	}
	    	
	    	for(int i=0;i<tempRecs.size();i++){
	    		if(tempRecs.get(i).getX()>currentUncoveredPosition){
	    			return (int) tempRecs.get(i).getX();
	    		}
	    	}
	    	
		 
		 return -1;
		 

		 
	 }
	
	 //////////////////////////////////////////////////////////
	
	 //sets the minimum number of sensor required for a given radius
	 public void calculateSensorParameters(){
	    
	    	numOfSensors= fieldSize/(radiusOfSensors*2);
	    	//in case the calculation above rounds down the minimum required sensors to cover the field
	    if(numOfSensors*(radiusOfSensors*2)< fieldSize && extraSensors==0){
	    	numOfSensors=numOfSensors+1;
	    }
	    		
	    	//}
	    }
	 
	 ///////////////////////////////////////////////////////
	 public void sortRectangles(){
	    	int currentPositionToChange=0;
	    	int currentSmallestIndex=0;
	    	
	    	for(int i=0;i<recs.size();i++){
	    		currentPositionToChange=i;
	    		currentSmallestIndex=i;
	    		
	    			for(int j=i;j<recs.size();j++){
	    				//currentSmallestIndex=recs.get(j).getX();
	    				if(recs.get(j).getX()< recs.get(currentSmallestIndex).getX()){
	    				currentSmallestIndex=j;
	    				}
	    			}
	    			Rectangle temp = recs.get(i);//saving the rectangle at current position
	    			//replacing the rectangle at current position with smaller x position rectangle
	    			recs.set(i, recs.get(currentSmallestIndex));
	    			recs.set(currentSmallestIndex, temp);
	    	}
	    
	    }
	 ///////////////////////////////////////////////////////////
	
}
