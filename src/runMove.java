import java.awt.Color;
import java.util.Scanner;

import javax.swing.JFrame;



public class runMove {

	public static void main(String args[]){
		int radius=1;
		int extra=0; 
		String algorithm="";
		Scanner reader = new Scanner(System.in);  
		System.out.println("");
		System.out.println("Enter a number for radius:1 or higher ");
		radius = reader.nextInt();
			
		System.out.println("Enter a number for extra sensors:0 or higher ");
		extra = reader.nextInt();
	
		System.out.println("Enter name of algorithm to run: simple or rigid");
		algorithm = reader.next();
		
		move m = new move(radius, extra, algorithm);
		JFrame f = new JFrame();
		f.add(m);
		f.setVisible(true);
		f.setSize(500, 400);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setForeground(Color.BLACK);
		f.setResizable(false);
	}
	
}
