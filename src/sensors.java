import java.awt.Rectangle;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class sensors {
	
	private int tracker[];
	private int totalMovement;
	private int sensorTracker[];
	private ArrayList<Integer> iterations;
	private File log ;
	private int radius;
	
	public sensors(int trackerSize, int sensors, int sensorRadius){
		tracker= new int[trackerSize];
		sensorTracker= new int[sensors];
		iterations = new ArrayList<Integer>();
	    log = new File("log.txt");
		totalMovement=0;
		radius=sensorRadius;
	}
	
	//initializes the tracker array components to zero values( zero meaning no place is initially assumed covered
	//on field and 1 meaning the position is covered on field by some sensor as the algorithm updates field).
	public void initiate(){
		for (int i=0;i<tracker.length;i++){
			tracker[i]=0;
		}
		
		for (int i=0;i<sensorTracker.length;i++){
			sensorTracker[i]=0;
		}
		
	}
	//turns all the components in the tracker array from the first zero position found, to a value reflecting the 
	//size of the sensor.//for rigid
	public void updateTrackerAtPosition(int sizeOfSensor){
		int position=0;
		for(int i=0;i<tracker.length;i++){
			if(tracker[i]==0){
				position=i;
				break;
			}
		}
		
		for(int i=position;i<(position+sizeOfSensor);i++){
			if(i<tracker.length){
			tracker[i]=1;
			}
		}
	}
	
	public void resetForNewIteration(){
		resetTracker();
		
		for (int i=0;i<sensorTracker.length;i++){
			sensorTracker[i]=0;
		}
		totalMovement=0;
		
	}
	
	public void averageIterationData(){
		int temp=0;
		for (int i=0;i<iterations.size();i++){
			temp= temp +iterations.get(i);
		}
		
		System.out.println(" Average is :" + (temp/iterations.size()));
	}
	
	public void updateIterations(){
		iterations.add(totalMovement);
	}
	public ArrayList<Integer> getIterations(){
		return iterations;
	}
	//updates which sensors have been moved
	public void updateSensors(int sensorID){
		sensorTracker[sensorID]=1;
	}
	
	//return a boolean value if a sensor has been moved
	public boolean sensorMoved(int sensorID){
		if(sensorTracker[sensorID]==1){
			return true;
			}
		
		
		return false;
	}
	
	public boolean allSensorsMoved(){
		for(int i=0;i<sensorTracker.length;i++){
			if(sensorTracker[i]==0){
				return false;
			}
		}
		
		return true;
	}
	
	//for simple coverage 
	public void resetTracker(){
		for (int i=0;i<tracker.length;i++){
			tracker[i]=0;
		}
	}
	//for simple coverage
	public void updateTrackerPositions(int sizeOfSensor, int position){
	
		
		
		for(int i=0;i<tracker.length;i++){
		
			if(position==i){
				for(int j=position;j<=(position+sizeOfSensor);j++){
					if(j<tracker.length){
					tracker[j]=1;
					}
				}
			}
			
		}
		
	}
	
	public void updateMovement(int moved){
		totalMovement=totalMovement+moved;
	}
	
	public int[] getTracker(){
		return tracker;
	}
	
	public int getMovement(){
		return totalMovement;
	}
	//returns the next uncovered x position on the field using tracker
	public int nextUncoveredXposition(){
		
		for(int i=0;i<tracker.length;i++){
			if(tracker[i]==0){
				return i;
			}
		}
		return -1;
	}
	
	public boolean checkTracker(){
		
		for(int i=0;i<tracker.length;i++){
			if(tracker[i]==0){
				return false;
			}
		}
		
		
		return true;
	}
		
	public void outputData(){
		
		int temp=0;
		for (int i=0;i<iterations.size();i++){
			temp= temp +iterations.get(i);
		}
		
	    try{
	    if(log.exists()==false){
	            log.createNewFile();
	    }
	    PrintWriter out = new PrintWriter(new FileWriter(log, true));
	    out.println();
    	out.println();
    	out.println("Radius :" + radius);
    	out.println("Iterations: ");
    	out.println();
	    for(int i=0;i<iterations.size();i++){
	    	if(i<(iterations.size()-1)){
	    		out.append(iterations.get(i)+" + ");	
	    	}
	    	if(i==(iterations.size()-1)){
	    		out.append(iterations.get(i)+" ");
	    	}

	    }
	    out.append(" = " + temp);
	    out.println();
	    out.println("Average :" + (temp/iterations.size()));
	    //out.append();
	    out.close();
	    }catch(IOException e){
	        System.out.println("COULD NOT LOG!!");
	    }
	}
	
	
}
