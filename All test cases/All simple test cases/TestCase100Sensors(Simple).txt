

Radius :5
Iterations: 

43 + 1194 + 82 + 119 + 87 + 41 + 80 + 113 + 74 + 172 + 158 + 83 + 49 + 117 + 134 + 62 + 83 + 104 + 65 + 79 + 162 + 77 + 101 + 80 + 60 + 51 + 64 + 81 + 110 + 84  = 3809
Average :126


Radius :10
Iterations: 

11 + 3 + 16 + 14 + 5 + 10 + 11 + 9 + 24 + 8 + 22 + 14 + 2 + 6 + 4 + 16 + 16 + 14 + 3 + 0 + 10 + 28 + 26 + 18 + 23 + 11 + 12 + 19 + 15 + 14  = 384
Average :12


Radius :15
Iterations: 

9 + 8 + 4 + 14 + 6 + 5 + 2 + 26 + 12 + 5 + 4 + 4 + 5 + 26 + 7 + 7 + 3 + 3 + 8 + 7 + 30 + 4 + 6 + 10 + 3 + 16 + 37 + 6 + 6 + 8  = 291
Average :9


Radius :20
Iterations: 

6 + 9 + 2 + 5 + 6 + 10 + 5 + 7 + 13 + 15 + 13 + 13 + 2 + 12 + 16 + 22 + 6 + 18 + 14 + 11 + 8 + 2 + 3 + 4 + 5 + 9 + 19 + 5 + 2 + 1  = 263
Average :8


Radius :25
Iterations: 

3 + 1 + 8 + 9 + 7 + 6 + 5 + 5 + 18 + 13 + 6 + 15 + 16 + 8 + 10 + 2 + 13 + 5 + 17 + 9 + 5 + 21 + 1 + 12 + 9 + 7 + 15 + 6 + 8 + 18  = 278
Average :9


Radius :30
Iterations: 

31 + 6 + 4 + 10 + 7 + 4 + 6 + 14 + 9 + 13 + 5 + 8 + 18 + 9 + 6 + 9 + 10 + 4 + 4 + 14 + 6 + 25 + 7 + 16 + 3 + 2 + 11 + 15 + 5 + 6  = 287
Average :9


Radius :35
Iterations: 

6 + 0 + 1 + 8 + 5 + 6 + 3 + 6 + 15 + 5 + 2 + 5 + 8 + 7 + 12 + 6 + 15 + 19 + 9 + 7 + 4 + 7 + 13 + 1 + 2 + 0 + 3 + 0 + 7 + 17  = 199
Average :6


Radius :40
Iterations: 

9 + 11 + 11 + 7 + 1 + 4 + 6 + 3 + 4 + 0 + 9 + 4 + 4 + 9 + 11 + 3 + 14 + 2 + 7 + 9 + 1 + 13 + 11 + 4 + 6 + 6 + 1 + 10 + 15 + 1  = 196
Average :6


Radius :45
Iterations: 

2 + 4 + 10 + 8 + 13 + 3 + 3 + 7 + 3 + 10 + 8 + 0 + 11 + 11 + 10 + 0 + 20 + 12 + 6 + 8 + 15 + 7 + 6 + 5 + 9 + 16 + 11 + 8 + 8 + 10  = 244
Average :8


Radius :50
Iterations: 

3 + 9 + 7 + 10 + 6 + 9 + 7 + 1 + 0 + 6 + 4 + 0 + 6 + 18 + 2 + 12 + 6 + 1 + 10 + 3 + 8 + 11 + 3 + 0 + 10 + 5 + 7 + 11 + 5 + 5  = 185
Average :6



