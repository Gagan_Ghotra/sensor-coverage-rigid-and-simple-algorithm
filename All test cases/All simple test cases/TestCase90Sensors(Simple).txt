

Radius :5
Iterations: 

56 + 108 + 153 + 87 + 147 + 104 + 55 + 111 + 171 + 70 + 96 + 160 + 175 + 147 + 159 + 92 + 119 + 46 + 109 + 116 + 154 + 127 + 1337 + 117 + 86 + 113 + 1344 + 89 + 129 + 84  = 5861
Average :195


Radius :10
Iterations: 

0 + 27 + 11 + 30 + 15 + 10 + 32 + 28 + 35 + 12 + 22 + 21 + 7 + 1 + 28 + 12 + 20 + 17 + 19 + 20 + 23 + 61 + 9 + 6 + 20 + 22 + 25 + 22 + 49 + 27  = 631
Average :21


Radius :15
Iterations: 

3 + 10 + 13 + 5 + 16 + 9 + 9 + 21 + 17 + 3 + 12 + 3 + 6 + 7 + 23 + 13 + 18 + 24 + 16 + 18 + 12 + 8 + 2 + 14 + 17 + 9 + 13 + 5 + 18 + 8  = 352
Average :11


Radius :20
Iterations: 

5 + 13 + 12 + 1 + 4 + 16 + 11 + 5 + 2 + 7 + 14 + 9 + 6 + 5 + 16 + 10 + 8 + 2 + 23 + 8 + 3 + 5 + 14 + 18 + 8 + 0 + 3 + 1 + 9 + 7  = 245
Average :8


Radius :25
Iterations: 

9 + 11 + 3 + 14 + 2 + 15 + 1 + 0 + 2 + 5 + 5 + 13 + 15 + 6 + 7 + 4 + 8 + 10 + 2 + 7 + 19 + 20 + 0 + 6 + 2 + 16 + 1 + 7 + 9 + 8  = 227
Average :7


Radius :30
Iterations: 

6 + 9 + 8 + 2 + 3 + 12 + 4 + 4 + 1 + 7 + 15 + 14 + 0 + 30 + 6 + 1 + 8 + 12 + 8 + 3 + 30 + 12 + 4 + 11 + 24 + 3 + 4 + 21 + 9 + 7  = 278
Average :9


Radius :35
Iterations: 

7 + 4 + 1 + 19 + 5 + 16 + 17 + 9 + 15 + 10 + 6 + 18 + 5 + 10 + 2 + 12 + 10 + 13 + 10 + 29 + 2 + 4 + 4 + 3 + 16 + 1 + 9 + 6 + 1 + 6  = 270
Average :9


Radius :40
Iterations: 

2 + 2 + 0 + 9 + 7 + 12 + 2 + 22 + 3 + 7 + 10 + 12 + 11 + 17 + 4 + 13 + 4 + 10 + 5 + 3 + 6 + 20 + 3 + 10 + 14 + 4 + 13 + 18 + 2 + 3  = 248
Average :8


Radius :45
Iterations: 

14 + 5 + 10 + 0 + 1 + 21 + 16 + 10 + 4 + 7 + 12 + 13 + 8 + 6 + 12 + 7 + 7 + 5 + 0 + 4 + 4 + 5 + 11 + 4 + 19 + 5 + 12 + 5 + 6 + 6  = 239
Average :7


Radius :50
Iterations: 

3 + 4 + 8 + 4 + 5 + 3 + 6 + 21 + 7 + 21 + 0 + 9 + 6 + 9 + 13 + 3 + 11 + 9 + 5 + 5 + 24 + 19 + 8 + 9 + 5 + 13 + 17 + 1 + 11 + 13  = 272
Average :9
